<?php

use Illuminate\Support\Facades\Route;
use Gralias\GConfig\Http\Controllers\GConfigController;

Route::get('g.config', [GConfigController::class, 'index'])->name('g.config.index');
