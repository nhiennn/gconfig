<?php

namespace Gralias\GConfig;

use Gralias\GConfig\Models\GConfig as GConfigModel;

class GConfig
{
    public function setName(string $name = null, $value = null) {
        if (is_null($name)) return null;

        GConfigModel::query()->updateOrCreate([
            'name' => $name
        ], [
            'name' => $name,
            'value' => $value
        ]);

        return "Setting $name with value $value successfully!";
    }

    public function delName(string $name = null) {
        if (is_null($name)) return null;

        $config = GConfigModel::query()->where([
            'name' => $name
        ]);

        if ($config->count()) {
            $config->delete();

            return "Setting with name $name has been deleted successfully!";
        }
        return "Have no setting with name $name!";
    }
}
