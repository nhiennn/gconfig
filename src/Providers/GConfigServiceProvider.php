<?php

namespace Gralias\GConfig\Providers;

use Gralias\GConfig\Console\GConfigConsole;
use Gralias\GConfig\Console\GConfigDeleteConsole;
use Gralias\GConfig\Console\GConfigInstallConsole;
use Gralias\GConfig\GConfig;
use Illuminate\Support\ServiceProvider;

class GConfigServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('g.config', function($app) {
            return new GConfig();
        });
    }

    public function boot() {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GConfigConsole::class,
                GConfigDeleteConsole::class,
                GConfigInstallConsole::class
            ]);
        }

        $this->publishes([
            __DIR__ . '/../database/migrations/create_g_config_table.php' => database_path('migrations/' . date('Y_m_d_His', time()) . '_create_g_config_table.php'),
        ], 'gconfig-provider');

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');
    }
}
