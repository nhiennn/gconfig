<?php

namespace Gralias\GConfig\Models;

use Illuminate\Database\Eloquent\Model;

class GConfig extends Model
{
    protected $table = 'g_config';

    protected $fillable = ['name', 'value'];
}
