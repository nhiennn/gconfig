<?php

namespace Gralias\GConfig\Console;

use Illuminate\Console\Command;

use Gralias\GConfig\Facades\GConfig;

class GConfigConsole extends Command
{
    protected $signature = 'gconfig:set';

    protected $description = 'Set data to GConfig';

    public function handle()
    {
        $name = $this->ask('What is the name of config?');
        $value = $this->ask('And value is?');

        $this->info(GConfig::setName($name, $value));
    }
}
