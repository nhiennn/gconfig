<?php

namespace Gralias\GConfig\Console;

use Illuminate\Console\Command;

use Gralias\GConfig\Facades\GConfig;

class GConfigDeleteConsole extends Command
{
    protected $signature = 'gconfig:delete
                            {name : Name of config}';

    protected $description = 'Delete config';

    public function handle()
    {
        $this->info(GConfig::delName($this->argument('name')));
    }
}
