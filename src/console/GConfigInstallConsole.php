<?php

namespace Gralias\GConfig\Console;

use Illuminate\Console\Command;

class GConfigInstallConsole extends Command
{
    protected $signature = 'gconfig:install';

    protected $description = 'Install the GConfig';

    public function handle()
    {
        $this->info('Installing GConfig...');

        $this->callSilent('vendor:publish', ['--tag' => 'gconfig-provider']);

        $this->info('Installed GConfig');
    }
}
