<?php

namespace Gralias\GConfig\Facades;

use Illuminate\Support\Facades\Facade;

class GConfig extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'g.config';
    }
}
